#include <iostream>
#include <vector>

using namespace std;

class A
{
	private:
		vector<int> vec;
	public:
		A() : vec(0)
			{}
		A(vector<int> v) : vec(v)
			{}

		void view()
		{
			cout << vec[1] << endl;
		}

		A operator+(vector<int>& v2)
		{
			int size = v2.size();
			cout << size << "\n";
			int posForInsert = size;
			int pos = 0;
			vector<int>::iterator it;
			it = vec.begin()+size;
			while (size != 0)
			{
				cout << v2.at(pos) << "\n";
				vec.insert(it, v2.at(pos));
				it = vec.begin()+1;
				pos = pos + 1;
				size = size - 1;
				posForInsert = posForInsert + 1;
			}
			return A(vec);
		}

};

int main()
{
	vector<int> myVector(10, 100);
	A vec1(myVector);
	vector<int> vec2(10,200);
	A vec3 = vec1 + vec2;
	vec3.view();
	return 0;
}