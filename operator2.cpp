#include <iostream>
#include <vector>

using namespace std;

class A
{
private:
        vector<int> v;
public:

  A()
        {
                v = vector<int>();
        }

        void view()
  {
                for (auto &e : this->v)
                        cout << e << "\n";
        }

        A& operator+(vector<int> &v)
        {
                this->v.insert(this->v.end(), v.begin(), v.end());
                return *this;
        }

        int& operator[](int i)
        {
            if (i > v.size())
            {
                cout << "ERROR index out range\n";
            }
            
            return v.at(i);
        }
};

int main()
{
        A *a = new A();
        vector<int> vector1(10, 200);
        vector<int> vector2(10, 300);
        *a = *a + vector1;
        int b = a[0];
        //cout << a[0] << "\n"; 
        a->view();

        *a = *a + vector2;
        a->view();

        delete a;
  return 0;
}